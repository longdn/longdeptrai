﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceT.Models;

namespace InsuranceT.Controllers
{
    public class PolicyApprovalsController : Controller
    {
        private Model1 db = new Model1();

        // GET: PolicyApprovals
        public ActionResult Index()
        {
            var policyApprovals = db.PolicyApprovals.Include(p => p.PolicyRequest);
            return View(policyApprovals.ToList());
        }

        // GET: PolicyApprovals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyApproval policyApproval = db.PolicyApprovals.Find(id);
            if (policyApproval == null)
            {
                return HttpNotFound();
            }
            return View(policyApproval);
        }

        // GET: PolicyApprovals/Create
        public ActionResult Create()
        {
            ViewBag.PolicyRequestId = new SelectList(db.PolicyRequests, "Id", "Status");
            return View();
        }

        // POST: PolicyApprovals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PolicyRequestId,Pstartdate,Penddate,Status,Reason")] PolicyApproval policyApproval)
        {
            if (ModelState.IsValid)
            {
                db.PolicyApprovals.Add(policyApproval);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PolicyRequestId = new SelectList(db.PolicyRequests, "Id", "Status", policyApproval.PolicyRequestId);
            return View(policyApproval);
        }

        // GET: PolicyApprovals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyApproval policyApproval = db.PolicyApprovals.Find(id);
            if (policyApproval == null)
            {
                return HttpNotFound();
            }
            ViewBag.PolicyRequestId = new SelectList(db.PolicyRequests, "Id", "Status", policyApproval.PolicyRequestId);
            return View(policyApproval);
        }

        // POST: PolicyApprovals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PolicyRequestId,Pstartdate,Penddate,Status,Reason")] PolicyApproval policyApproval)
        {
            if (ModelState.IsValid)
            {
                db.Entry(policyApproval).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PolicyRequestId = new SelectList(db.PolicyRequests, "Id", "Status", policyApproval.PolicyRequestId);
            return View(policyApproval);
        }

        // GET: PolicyApprovals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PolicyApproval policyApproval = db.PolicyApprovals.Find(id);
            if (policyApproval == null)
            {
                return HttpNotFound();
            }
            return View(policyApproval);
        }

        // POST: PolicyApprovals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PolicyApproval policyApproval = db.PolicyApprovals.Find(id);
            db.PolicyApprovals.Remove(policyApproval);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
