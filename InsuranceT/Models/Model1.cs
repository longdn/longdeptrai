namespace InsuranceT.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<policy> policies { get; set; }
        public virtual DbSet<PolicyApproval> PolicyApprovals { get; set; }
        public virtual DbSet<PolicyRequest> PolicyRequests { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<Admin>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<Company>()
                .Property(e => e.CompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<Company>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Company>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Company>()
                .Property(e => e.CompanyURL)
                .IsUnicode(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.policies)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.designation)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.salary)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Employee>()
                .Property(e => e.firstname)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.lastname)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.contactno)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.country)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.PolicyRequests)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<policy>()
                .Property(e => e.policyname)
                .IsUnicode(false);

            modelBuilder.Entity<policy>()
                .Property(e => e.policydesc)
                .IsUnicode(false);

            modelBuilder.Entity<policy>()
                .Property(e => e.amount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<policy>()
                .Property(e => e.emi)
                .HasPrecision(18, 0);

            modelBuilder.Entity<policy>()
                .Property(e => e.medicalid)
                .IsUnicode(false);

            modelBuilder.Entity<policy>()
                .HasMany(e => e.PolicyRequests)
                .WithRequired(e => e.policy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PolicyApproval>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<PolicyApproval>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<PolicyRequest>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<PolicyRequest>()
                .HasMany(e => e.PolicyApprovals)
                .WithRequired(e => e.PolicyRequest)
                .WillCascadeOnDelete(false);
        }
    }
}
