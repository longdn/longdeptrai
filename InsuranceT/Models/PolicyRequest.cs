namespace InsuranceT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PolicyRequest")]
    public partial class PolicyRequest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PolicyRequest()
        {
            PolicyApprovals = new HashSet<PolicyApproval>();
        }

        public int Id { get; set; }

        public DateTime RequestDate { get; set; }

        public int EmployeeId { get; set; }

        public int PolicyId { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual policy policy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PolicyApproval> PolicyApprovals { get; set; }
    }
}
