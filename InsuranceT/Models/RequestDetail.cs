﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsuranceT.Models
{
    public class RequestDetail
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Emi { get; set; }
        public DateTime RequestDate { get; set; }
        public string CompanyName { get; set; }
        public int? CompanyId { get; set; }
    }
}