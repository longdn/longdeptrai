﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InsuranceT.Models
{
    public class EmployeeLogin
    {
        [System.ComponentModel.DisplayName("User Name :")]
        [Required(ErrorMessage = "UserName is required.")]
        public string username { get; set; }
        [System.ComponentModel.DisplayName("PassWord :")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "PassWord is required.")]
        public string password { get; set; }
    }
}